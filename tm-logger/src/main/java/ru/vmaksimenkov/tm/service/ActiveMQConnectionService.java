package ru.vmaksimenkov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IActiveMQConnectionService;

import javax.jms.Connection;
import java.util.Arrays;

import static ru.vmaksimenkov.tm.constant.Const.URL;

@Getter
public class ActiveMQConnectionService implements IActiveMQConnectionService {

    @NotNull
    final ActiveMQConnectionFactory connectionFactory;
    @NotNull
    final Connection connection;

    @SneakyThrows
    public ActiveMQConnectionService() {
        connectionFactory = new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustedPackages(Arrays.asList("ru.vmaksimenkov.tm.jms", "ru.vmaksimenkov.tm.enumerated", "java.util"));
        connection = connectionFactory.createConnection();
        connection.start();
    }

}
