package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.vmaksimenkov.tm.api.endpoint.ISessionEndpoint;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @Override
    @WebMethod
    public boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        try {
            sessionRecordService.close(session);
            return true;
        } catch (@NotNull final AccessDeniedException e) {
            return false;
        }
    }

    @Nullable
    @Override
    @WebMethod
    public UserRecord getUser(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        return sessionRecordService.getUser(session);
    }

    @Nullable
    @Override
    @WebMethod
    public SessionRecord openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        return sessionRecordService.open(login, password);
    }
}
