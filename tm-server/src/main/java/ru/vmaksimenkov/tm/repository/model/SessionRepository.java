package ru.vmaksimenkov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.api.repository.model.ISessionRepository;
import ru.vmaksimenkov.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public class SessionRepository extends AbstractBusinessRepository<Session> implements ISessionRepository {

    @Override
    public void clear(@Nullable final String userId) {
        entityManager.createQuery("DELETE Session where user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE Session").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Session WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Session WHERE user.id = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        return entityManager.createQuery("FROM Session WHERE user.id = :userId", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("FROM Session", Session.class).setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public Session findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(entityManager.createQuery("FROM Session WHERE user.id = :userId AND id = :id", Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Session findById(@Nullable final String id) {
        return getEntity(entityManager.createQuery("FROM Session WHERE user.id = :userId AND id = :id", Session.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Session WHERE user.id = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return entityManager.createQuery("SELECT COUNT(*) FROM Session", Long.class).setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
