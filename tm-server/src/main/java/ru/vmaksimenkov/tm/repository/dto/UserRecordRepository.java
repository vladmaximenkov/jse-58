package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.dto.UserRecord;

import java.util.List;

@Repository
@Scope("prototype")
public class UserRecordRepository extends AbstractRecordRepository<UserRecord> implements IUserRecordRepository {

    @Override
    @Nullable
    public UserRecord findById(@NotNull final String id) {
        return getEntity(entityManager.createQuery("FROM UserRecord WHERE id = :id", UserRecord.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Override
    public void removeById(@Nullable final String id) {
        entityManager.createQuery("DELETE UserRecord WHERE id = :id")
                .setParameter(id, id).executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE UserRecord").executeUpdate();
    }

    @Override
    public boolean existsByEmail(@Nullable final String email) {
        return entityManager.createQuery("SELECT COUNT(*) FROM UserRecord WHERE email = :email", Long.class)
                .setParameter("email", email)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM UserRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) {
        return entityManager.createQuery("SELECT COUNT(*) FROM UserRecord WHERE login = :login", Long.class)
                .setParameter("login", login)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<UserRecord> findAll() {
        return entityManager.createQuery("FROM UserRecord", UserRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public UserRecord findByLogin(@Nullable final String login) {
        return getEntity(entityManager.createQuery("FROM UserRecord WHERE login = :login", UserRecord.class)
                .setParameter("login", login)
                .setMaxResults(1));
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager.createQuery("DELETE UserRecord WHERE login = :login")
                .setParameter("login", login).executeUpdate();
    }

    @Override
    public @NotNull Long size() {
        return entityManager.createQuery("SELECT COUNT(*) FROM UserRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
