package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.api.repository.IRepository;

import javax.persistence.EntityManager;

public class Repository implements IRepository {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void close() {
        entityManager.close();
    }

    @Override
    public void commit() {
        entityManager.getTransaction().commit();
    }

    @Override
    public void rollback() {
        entityManager.getTransaction().rollback();
    }

    @Override
    public void begin() {
        entityManager.getTransaction().begin();
    }

}
