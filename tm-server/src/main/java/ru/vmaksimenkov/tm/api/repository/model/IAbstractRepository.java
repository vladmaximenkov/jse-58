package ru.vmaksimenkov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.IRepository;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.List;

public interface IAbstractRepository<E extends AbstractEntity> extends IRepository {

    void add(@Nullable E entity);

    void add(@Nullable List<E> entities);

    void clear();

    boolean existsById(@Nullable String id);

    @Nullable
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    void remove(@Nullable E entity);

    void removeById(@Nullable String id);

    @NotNull
    Long size();

    void update(@Nullable E entity);

}
