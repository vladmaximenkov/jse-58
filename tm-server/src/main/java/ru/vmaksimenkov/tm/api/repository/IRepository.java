package ru.vmaksimenkov.tm.api.repository;

public interface IRepository {

    void close();

    void commit();

    void rollback();

    void begin();

}
