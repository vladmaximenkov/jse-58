package ru.vmaksimenkov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.service.IActiveMQConnectionService;
import ru.vmaksimenkov.tm.enumerated.EventType;
import ru.vmaksimenkov.tm.jms.LogMessage;

import javax.jms.*;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.apache.activemq.ActiveMQConnectionFactory.DEFAULT_BROKER_BIND_URL;
import static ru.vmaksimenkov.tm.constant.PropertyConst.SUBJECT;
import static ru.vmaksimenkov.tm.constant.PropertyConst.URL;

@Getter
public class ActiveMQConnectionService implements IActiveMQConnectionService {

    private static final int THREAD_NUMBER = 3;
    @NotNull
    public final Connection connection;
    @NotNull
    private final ActiveMQConnectionFactory connectionFactory;
    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_NUMBER);

    @SneakyThrows
    public ActiveMQConnectionService() {
        BrokerService broker = new BrokerService();
        broker.addConnector(DEFAULT_BROKER_BIND_URL);
        broker.setPersistent(false);
        broker.start();
        connectionFactory = new ActiveMQConnectionFactory(URL);
        connection = connectionFactory.createConnection();
        connection.start();
    }

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EventType event) {
        @NotNull final Session session = getConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(SUBJECT);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @Nullable final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(entity);
        @NotNull final LogMessage logMessage = new LogMessage(entity.getClass().getSimpleName(), event, json, new Date(System.currentTimeMillis()));
        @NotNull final ObjectMessage message = session.createObjectMessage(logMessage);
        producer.send(message);
    }

    public void send(@NotNull final Object entity, @NotNull final EventType event) {
        es.submit(() -> sendMessage(entity, event));
    }

}
