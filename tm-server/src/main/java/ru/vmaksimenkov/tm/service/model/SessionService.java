package ru.vmaksimenkov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.repository.model.ISessionRepository;
import ru.vmaksimenkov.tm.api.repository.model.IUserRepository;
import ru.vmaksimenkov.tm.api.service.model.ISessionService;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.user.AccessDeniedException;
import ru.vmaksimenkov.tm.model.Session;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.repository.model.SessionRepository;
import ru.vmaksimenkov.tm.repository.model.UserRepository;
import ru.vmaksimenkov.tm.service.PropertyService;
import ru.vmaksimenkov.tm.util.HashUtil;
import ru.vmaksimenkov.tm.util.SignatureUtil;

import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
public final class SessionService extends AbstractBusinessService<Session, SessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Override
    protected SessionRepository getRepository() {
        return context.getBean(SessionRepository.class);
    }

    @NotNull
    public UserRepository getUserRepository() {
        return context.getBean(UserRepository.class);
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @NotNull final IUserRepository repository = getUserRepository();
        try {
            repository.begin();
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return false;
            if (user.isLocked()) return false;
            @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
            if (isEmpty(passwordHash)) return false;
            return passwordHash.equals(user.getPasswordHash());
        } finally {
            repository.close();
        }
    }

    @Override
    public void close(@NotNull final Session session) {
        validate(session);
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.remove(session);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAll(@NotNull final Session session) {
        validate(session);
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.clear(session.getUser().getId());
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> getListSession(@NotNull final Session session) {
        validate(session);
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            return repository.findAll(session.getUser().getId());
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User getUser(@NotNull final Session session) {
        @NotNull final String userId = getUserId(session);
        @NotNull final IUserRepository repository = getUserRepository();
        try {
            repository.begin();
            return repository.findById(userId);
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        validate(session);
        return session.getUser().getId();
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check || isEmpty(login)) return null;
        @NotNull final IUserRepository repository = getUserRepository();
        try {
            repository.begin();
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return null;
            @NotNull final Session session = new Session();
            session.setUser(user);
            session.setTimestamp(System.currentTimeMillis());
            add(session);
            repository.commit();
            return sign(session);
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        update(session);
        return session;
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUser().getId();
        @NotNull final IUserRepository repository = getUserRepository();
        try {
            repository.begin();
            @Nullable final User user = repository.findById(userId);
            if (user == null) throw new AccessDeniedException();
            if (user.getRole() == null) throw new AccessDeniedException();
            if (!role.equals(user.getRole())) throw new AccessDeniedException();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUser().getId())) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        try {
            sessionRepository.begin();
            if (!sessionRepository.existsById(session.getId())) throw new AccessDeniedException();
        } finally {
            sessionRepository.close();
        }
    }

}
