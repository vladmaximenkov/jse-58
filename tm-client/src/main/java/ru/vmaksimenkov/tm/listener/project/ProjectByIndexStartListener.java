package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class ProjectByIndexStartListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by index";
    }

    @NotNull
    @Override
    public String command() {
        return "project-start-by-index";
    }

    @Override
    @EventListener(condition = "@projectByIndexStartListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        projectEndpoint.startProjectByIndex(sessionService.getSession(), TerminalUtil.nextNumber());
    }

}
