package ru.vmaksimenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.entity.NoProjectsException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class ProjectByIdFinishListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project by id";
    }

    @NotNull
    @Override
    public String command() {
        return "project-finish-by-id";
    }

    @Override
    @EventListener(condition = "@projectByIdFinishListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        if (projectEndpoint.countProject(sessionService.getSession()) < 1)
            throw new NoProjectsException();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        projectEndpoint.finishProjectById(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
