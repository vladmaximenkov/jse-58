package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks";
    }

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    @EventListener(condition = "@taskClearListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK CLEAR]");
        taskEndpoint.clearTask(sessionService.getSession());
    }

}
