package ru.vmaksimenkov.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.listener.AbstractListener;

@Component
public final class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String argument() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info";
    }

    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @Override
    @EventListener(condition = "@aboutListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}
