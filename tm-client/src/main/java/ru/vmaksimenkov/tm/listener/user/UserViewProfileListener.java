package ru.vmaksimenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.UserEndpoint;
import ru.vmaksimenkov.tm.endpoint.UserRecord;
import ru.vmaksimenkov.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @NotNull
    @Override
    public String command() {
        return "user-view";
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final UserRecord user = userEndpoint.viewUser(sessionService.getSession());
        if (user == null) return;
        showUser(user);
    }

}
