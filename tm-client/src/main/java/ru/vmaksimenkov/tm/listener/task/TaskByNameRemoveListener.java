package ru.vmaksimenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByNameRemoveListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name";
    }

    @NotNull
    @Override
    public String command() {
        return "task-remove-by-name";
    }

    @Override
    @EventListener(condition = "@taskByNameRemoveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        taskEndpoint.removeTaskByName(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
