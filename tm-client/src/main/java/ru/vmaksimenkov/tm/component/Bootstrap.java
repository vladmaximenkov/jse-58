package ru.vmaksimenkov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.event.ConsoleEvent;
import ru.vmaksimenkov.tm.exception.system.UnknownCommandException;
import ru.vmaksimenkov.tm.listener.AbstractListener;
import ru.vmaksimenkov.tm.service.LoggerService;
import ru.vmaksimenkov.tm.util.SystemUtil;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.vmaksimenkov.tm.constant.Constant.EXIT_COMMAND;
import static ru.vmaksimenkov.tm.constant.Constant.PID_FILENAME;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    protected FileScanner fileScanner;

    @NotNull
    @Autowired
    protected LoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void init() {
        initPID();
        fileScanner.init();
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILENAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILENAME);
        file.deleteOnExit();
    }

    public void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractListener listener = getListenerByArg(arg);
        if (listener == null) return;
        publisher.publishEvent(new ConsoleEvent(listener.command()));
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void run(@Nullable final String... args) {
        loggerService.debug("Debug mode");
        loggerService.info("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        init();
        @NotNull String command = "";
        while (!EXIT_COMMAND.equals(command)) {
            System.out.println("Enter command: ");
            try {
                command = TerminalUtil.nextLine();
                if (command.isEmpty()) return;
                loggerService.command(command);
                boolean check = false;
                for (@NotNull final AbstractListener listener : listeners) {
                    if (command.equals(listener.command())) check = true;
                }
                if (command.equals(EXIT_COMMAND)) check = true;
                if (!check) throw new UnknownCommandException(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.err.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
        System.exit(0);
    }

    @Nullable
    private AbstractListener getListenerByArg(@Nullable final String arg) {
        if (isEmpty(arg)) return null;
        for (@NotNull final AbstractListener listener : listeners) {
            if (arg.equals(listener.argument())) return listener;
        }
        return null;
    }

}
